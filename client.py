#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):
		self.catalog = MessageCatalog(catalog_path)
		if debug:
			print(self.catalog)
		self.r = None
		self.debug = debug
		self.protocol = defaultProtocol

	@classmethod
	def withProtocol(cls, host):
		res = host.find("://") > 1
		return res

	def completeUrl(self, host, route = ""):
		if not Client.withProtocol(host):
			host = self.protocol + host
		if route != "":
			route = "/"+route
		return host+route

	#send an I said message to a host
	def say_something(self, host):
		m = Message()
		self.catalog.add_message(m)
		route = self.completeUrl(host, m.content)
		self.r = requests.post(route)
		if self.debug:
			print("POST  "+route + "→" + str(self.r.status_code))
			print(self.r.text)
		return self.r.status_code == 201

	#add to catalog all the covid from host server
	def get_covid(self, host):
		route = self.completeUrl(host,'/they-said')
		self.r = requests.get(route)
		res = self.r.status_code == 200
		if res:
			res = self.catalog.c_import(self.r.json())
		if self.debug:
			print("GET  "+ route + "→" + str(self.r.status_code))
			if res != False:
				print(str(self.r.json()))
		return res

	#send to server list of I said messages
	def send_history(self, host):
		route = self.completeUrl(host,'/they-said')
		self.catalog.purge(Message.MSG_ISAID)
		data = self.catalog.c_export_type(Message.MSG_ISAID)
		self.r = requests.post(route, json=data)
		if self.debug:
			print("POST  "+ route + "→" + str(self.r.status_code))
			print(str(data))
			print(str(self.r.text))
		return self.r.status_code == 201


if __name__ == "__main__":
	c = Client("client.json", True)
	c.say_something("localhost:5000")
	c.get_covid("localhost:5000")
	c.send_history("localhost:5000")
