#!/usr/bin/env python3
#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
import json
#My libraries
from message_catalog import MessageCatalog
from message import Message

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET'])
def index():
	pass ## TODO:

	#case 2 hear message
@app.route('/<msg>', methods=['POST'])
def add_heard(msg):
	pass ## TODO:
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST'])
def hospital():
	pass ## TODO:
#End hospital use case
#will only execute if this file is run
if __name__ == "__main__":
	catalog = MessageCatalog()
	app.run(host="0.0.0.0", debug=False)
