#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes


#for generation of messages
from random import random
from hashlib import sha256
import random as rd

class Message:
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):
		self.msg_type=msg_type
		self.msg=msg
		self.msg_date=msg_date

	#a method to compute the age of a message in days or in seconds
	def age(self, days=True):
		tps= time.time()/(60*60*24)
		days= tps-self.msg_date
		return(days)

	#a class method that generates a random message
	@classmethod
	def generate(self):
		import string
		ascii=string.ascii_letters
		a=[]
		for i in range (len(ascii)):
			a.append(ascii[i])
		a=a+['1','2','3','4','5','6','7','8','9','0']
		messa=rd.sample(a,4)
		b=""
		for i in range (len(messa)):
			b=b+str(messa[i])
		self.msg=b

	#a method to convert the object to string data
	def __str__(self):
		pass

	#export/import
	def m_export(self):

	def m_import(self, msg):

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
